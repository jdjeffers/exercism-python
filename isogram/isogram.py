def is_isogram(string):
    chars = {}

    for c in string.lower():
        if c in chars:
            return False
        if c != '-' and c != ' ':
            chars[c] = c
    return True
