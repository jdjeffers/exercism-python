def rna_map(c):
    if c == 'G':
        return 'C'
    elif c == 'C':
        return 'G'
    elif c == 'T':
        return 'A'
    elif c == 'A':
        return 'U'
    else:
        raise ValueError

def to_rna(dna_strand):
    return ''.join([rna_map(c) for c in dna_strand])
